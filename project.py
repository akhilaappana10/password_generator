import tkinter as tk
import random
import string

class PasswordGeneratorApp:
    def _init_(self, root):
        self.root = root
        root.geometry('600x400')
        self.root.title('PASSGENIE')
        self.label_strength = tk.Label(root, text="PASSWORD STRENGTH:")
        self.label_strength.place(x=50,y=150,width=1000,height=30)

        self.strength_var = tk.StringVar()
        self.strength_var.set("strong")

        #create font for all radiobutttons
        custom_font=("Arial",12,"bold")

        self.radio_strong = tk.Radiobutton(root, text="Strong", variable=self.strength_var, value="strong")
        self.radio_strong.place(x=550,y=200,width=200,height=20)

        self.radio_medium = tk.Radiobutton(root, text="Medium", variable=self.strength_var, value="medium")
        self.radio_medium.place(x=550,y=240,width=200,height=20)

        self.radio_easy = tk.Radiobutton(root, text="Easy", variable=self.strength_var, value="easy")
        self.radio_easy.place(x=550,y=280,width=200,height=20)

        self.label_length = tk.Label(root, text="PASSWORD LENGTH :")
        self.label_length.place(x=500,y=330,width=300,height=30)

        self.password_length = tk.Entry(root)
        self.password_length.place(x=500,y=390,width=300,height=30)

        self.generate_button = tk.Button(root, text="Get My Password", command=self.generate_password)
        self.generate_button.place(x=500,y=450,width=300,height=30)

        self.generated_password = tk.Label(root, text="")
        self.generated_password.place(x=500,y=500,width=200,height=50)
        
    def generate_password(self):
        strength = self.strength_var.get()
        length = int(self.password_length.get())

        if strength == "strong":
            characters = string.ascii_letters + string.digits + string.punctuation
        elif strength == "medium":
            characters = string.ascii_letters + string.digits
        else:
            characters = string.ascii_lowercase

        password = ''.join(random.choice(characters) for _ in range(length))

    
if __name__ == "__main__":
    root = tk.Tk()
    app = PasswordGeneratorApp()
    root.mainloop()
